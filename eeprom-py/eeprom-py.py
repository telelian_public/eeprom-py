#!/usr/bin/python3
import os
from time import sleep
from enum import Enum
from loguru import logger
from smbus2 import SMBus
import click

DEFAULT_ADDR = 0x53
CHIP_SIZE = 256

class I2CEEProm(SMBus):
    def __init__(self, bus_number=2, i2c_addr=DEFAULT_ADDR):
        super().__init__(bus=bus_number)
        self.i2c_addr = i2c_addr
        self._set_address(self.i2c_addr,force=True)

    def read(self):
        data = []
        block_size = 8
        for i in range(0, CHIP_SIZE, block_size):
            data += self.read_i2c_block_data(self.i2c_addr, i, block_size)

        data = bytes(data)
        return data

    def write(self, data):
        block_size = 8
        leng = len(data)
        if leng>256:
            return False
        if isinstance(data, str):
            logger.error(f'cannot use str, use bytes')
            return False


        blk = []
        for i in range(0,leng,block_size):
            if block_size < leng:
                blk.append(data[i:block_size+i])
            else:
                blk.append(data[i:leng])

        #logger.debug(blk)
        for i, data in enumerate(blk):
            logger.debug(f'i : {i}, data = {data}')
            self.write_i2c_block_data(self.i2c_addr, i * block_size, data)
            sleep(0.01)
        return True

#@click.group()
#@click.version_option(__init__.__version__)
#@click.option('--bus', default=2, help='i2c bus [default=2]')
#@click.option('--addr', default='0x53', help='eeprom address [default=0x53]')
#def cli(debug, bus, addr):
#    global eeprom
#    addr = int(addr, 16)
#    click.echo(f' {bus=} {addr=:x}')
#    eeprom = I2CEEProm(bus_number=bus, i2c_addr=addr)

    
if __name__ == '__main__':
    eeprom = I2CEEProm()
    
    #data = b'jdk100 123-456-789-10'
    #leng = len(data)
    #logger.debug(f'{data}, {leng}')
    #data = data + bytes(b'\xff'* (256 - leng))
    #logger.debug(f'{data}, {len(data)}')
    #logger.debug(eeprom.write(data))

    buf = eeprom.read()
    buf = bytes(buf)
    logger.debug(buf)

    

